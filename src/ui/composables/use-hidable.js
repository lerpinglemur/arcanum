import { useEventListener } from "@vueuse/core";
import { nextTick, onUnmounted, ref } from 'vue';

/**
 * Enable HTML element hiding.
 * 
 * Hidable elements must have the class selector: '.hidable'
 * 
 * - Clicking btnHide enters 'config' mode for hidden elements.
 * - In config mode, toggle buttons by hidable elements can be clicked
 * to toggle the hidden state of those elements.
 * - The elements are left greyed but not actually hidden while configuration
 * is in progress.
 * 
 * When btnHide is clicked again, config mode is exited and the
 * elements take on their new hidden states.
 * 
 * 
 * @param btnHide:Ref<HTMLElement | undefined> - button to toggle hidable config
 * @param parent:Ref<HTMLElement | undefined> - parent element of all hidable elements.
 */
export function useHidable( btnHide, parent) {

	const inConfig = ref(false);
	const hides = ref({});

    /// map element key to hidable boolean
	let newHides = null;

	/// Elements with .hidable. Stored when entering config in case the list changes
	/// in the middle of configuration.
    /// NodeListOf<Element>
	let hidableElms = null;

	const canShow = (it) => {
		return inConfig.value || !hides.value[it.id];
	}

	useEventListener(btnHide, 'click', (evt) => {
		evt.preventDefault();
		if (inConfig.value) {

			leaveConfig();

			/// apply the new hidden states.
			if (newHides) hides.value = newHides;
			newHides = null;

			evt.target.classList.remove('inConfig');

		}
		else {

			inConfig.value = true;
			evt.target.classList.add('inConfig');

			nextTick(enterConfig);

		}
	});

	const enterConfig = () => {

		newHides = hides.value;
		hides.value = {};

		const hideElms = parent.value?.querySelectorAll('.hidable');
		if (!hideElms) return;

		hidableElms = hideElms;

		for (let i = hideElms.length - 1; i >= 0; i--) {

			const h = hideElms[i];
			const key = h.dataset.key;
			if (key && newHides[key]) h.classList.add('inConfig', 'configHiding');
			else h.classList.add('inConfig');

			h.addEventListener('pointerdown', toggleHidden, true);

		}

	}

	/**
	 * Toggle the hidden status of an element.
	 * Element will not actually toggle visibility until
	 * leaving hide config.
	 */
	const toggleHidden = (evt) => {

		evt.preventDefault();
		evt.stopPropagation();

		const targ = evt.currentTarget;
		const id = targ?.dataset.key;

		if (!id) return;

		const v = newHides[id];
		if (v === undefined || v === null) {
			newHides[id] = true;
		}
		else newHides[id] = !v;

		if (!v) targ.classList.add('configHiding');
		else targ.classList.remove('configHiding');

	}

	/**
	 * Stop toggling hide on elements.
	 */
	const leaveConfig = () => {

		/// 
		const hideElms = hidableElms;
		hidableElms = null;

		if (!hideElms) return;

		// remove event listeners.
		for (let i = hideElms.length - 1; i >= 0; i--) {

			const h = hideElms[i];

			h.removeEventListener('pointerdown', toggleHidden, true);
			h.classList.remove('configHiding', 'inConfig');

		}

		inConfig.value = false;

	}

	onUnmounted(() => {
		leaveConfig();
	});

	return {
		inConfig,
		hides,
		canShow
	}
}