import Char from '../../chars/char';
import game from '../../game';
import { useRollOver } from '../../store/popups';
import { precise } from '../../util/format';
import { applyParams, FP, TYP_FUNC } from '../../values/consts';
import Range from '../../values/range';

export function getHealing(it) {
    return getHealingStr(it.healing || it.heal, it);

};

export function getHealingStr(heal, it, itemProp = 'item') {
    let mult = getHealingMult(it);
    let bonus = getHealingBonus(it);
    if (it.showinstanced) {
        mult = 1
        bonus = 0
    }
    if (!heal) return null;
    else if (typeof heal === 'number') return heal * mult + bonus;

    if (heal && heal.type === TYP_FUNC) {
        heal = heal.fn;
    }
    if (heal instanceof Function) {


        const rollOver = useRollOver().rollOver;
        
        let params = {
            [FP.GAME]: game.gdata,
            [FP.TARGET]: game.state.player,
            [FP.ACTOR]: rollOver.item instanceof Char ? rollOver.item : rollOver.source instanceof Char ? rollOver.source : game.state.player,
            [FP.CONTEXT]: game.state.player.context,
            [FP.ITEM]: it[itemProp]
        };
        return precise(applyParams(heal, params) * mult + bonus);
    }
    if (heal) {
        let healdisp
        if (heal instanceof Range) {
            healdisp = heal.instantiate();
            healdisp.add(bonus)
            healdisp.multiply(mult)
        }
        else {
            healdisp = heal * mult + bonus;
        }
        return healdisp.toString()
        //return healdisp.toString(this[itemProp]);
    }
    console.warn("Failed healing parse", heal);
    return null;
}

export function displayHealing(it) {
    if (!it) return false;

    const heal = it.healing || it.heal;
    return heal != null && (
        heal instanceof Function || heal?.type === TYP_FUNC || getHealingStr(heal, it)
    );
}

export function getHealingMult(it) {
    let PotencyMult = 1
    let Actor;

    const rollOver = useRollOver().rollOver;

    if (rollOver.item instanceof Char || rollOver.item.type == "monster") {
        Actor = rollOver.item
    }
    else if (rollOver.source instanceof Char) {
        Actor = rollOver.source
    }
    else Actor = game.state.player;
    if (Actor.context && it.potencies && Actor.id == "player") {
        for (let p of it.potencies) {
            let potency = Actor.context.state.getData(p)
            if (potency) {
                PotencyMult = PotencyMult * potency.damage.fn(Actor, game.state.player, game.state.player.context, potency)
            }
        }
    }
    return PotencyMult
}

export function getHealingBonus(it) {
    let HealingBonus = 0
    let Actor;

    const rollOver = useRollOver().rollOver;
    if (rollOver.item instanceof Char || rollOver.item.type == "monster") {
        Actor = rollOver.item
    }
    else if (rollOver.source instanceof Char) {
        Actor = rollOver.source
    }
    else Actor = game.state.player;
    if (Actor && Actor.getBonus) HealingBonus += Actor.getBonus(it.kind);
    if (it.bonus) HealingBonus += it.bonus;
    return HealingBonus
}