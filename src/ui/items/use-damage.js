import Game from '../../game';
import { precise } from '../../util/format';
import { applyParams, FP, TYP_FUNC } from '../../values/consts';
import Char from '../../chars/char';
import Range from '../../values/range';
import { useRollOver } from '../../store/popups';

export function useDamage(itemProp="item") {

    const {rollOver} = useRollOver();

    const getDamageStr = (dmg, it) => {
        let mult = getDamageMult(it);
        let bonus = getDamageBonus(it);
        if (it.showinstanced)
        {
            mult = 1
            bonus = 0
        }
        if(!dmg) return null;
        else if(typeof dmg === 'number') return dmg*mult+bonus;
    
        if ( dmg && dmg.type === TYP_FUNC) {
            dmg = dmg.fn;
        }
        if ( dmg instanceof Function ) {
            let params = {
                [FP.GAME]: Game.gdata, 
                [FP.TARGET]: Game.state.player, 
                [FP.ACTOR]: rollOver.item instanceof Char ? rollOver.item : rollOver.source instanceof Char ? rollOver.source : Game.state.player,
                [FP.CONTEXT]: Game.state.player.context,
                [FP.ITEM]: it
            };
            return precise(applyParams(dmg, params)*mult+bonus);
        }
        if ( dmg ) {
            let dmgdisp
            if(dmg instanceof Range)
            {
                dmgdisp = dmg.instantiate();
                dmgdisp.add(bonus)
                dmgdisp.multiply(mult)
            }
            else {
                dmgdisp = dmg*mult+bonus;
            }
            return dmgdisp.toString()
            //return dmgdisp.toString(this[itemProp]);
        }
        console.warn("Failed damage parse", dmg);
        return null;
    }
    
    function getDamageMult(it) {
        let PotencyMult = 1
        let actor
        if(rollOver.item instanceof Char||rollOver.item.type == "monster")
        {
            actor = rollOver.item
        }
        else if(rollOver.source instanceof Char)
        {
            actor = rollOver.source
        }
        else actor = Game.state.player;
        if (actor.context && it.potencies && actor.id =="player")
        {
            for (const p of it.potencies)
            {	
                let potency = actor.context.state.getData(p)
                if(potency){
                    PotencyMult = PotencyMult * potency.damage.fn( actor, Game.state.player, Game.state.player.context, potency )
                }
            }
        }
        return PotencyMult
    }
    
    function getDamageBonus(it) {
        let dmgBonus = 0
        let actor
        if(rollOver.item instanceof Char||rollOver.item.type == "monster")
        {
            actor = rollOver.item
        }
        else if(rollOver.source instanceof Char)
        {
            actor = rollOver.source
        }
        else actor = Game.state.player;
        if ( actor && actor.getBonus ) dmgBonus += actor.getBonus( it.kind );
        if ( it.bonus ) dmgBonus += it.bonus;
        return dmgBonus
    }

        return {
            getDamage(it) {
                return getDamageStr( it.damage || it.dmg, it);
            },
            displayDamage(it) {
                if(!it) return false;

                const dmg = it.damage || it.dmg;
                return dmg != null && (
                    dmg instanceof Function || dmg?.type === TYP_FUNC ||
                    getDamageStr(dmg, it)
                ); 
            },
            
        }
    }
    