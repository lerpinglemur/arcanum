import {computed} from 'vue';
import Base, { mergeClass } from "../items/base";
import { assign } from '../util/objecty';
import { addValues } from "../util/dataUtil";

/**
 * Currently used to make custom user spells.
 * Groups multiple GData items into a single item.
 */
export default class Group {

	toJSON() {

		const items = this.items;

		return {

			id: this.id,
			items: this.items.map(v => v.id),
			name: this.name,
			type: this.type,
			val: this.value,
			custom: 'group'

		}
	}

	get id() { return this._id; }
	set id(v) { this._id = v; }

	get name() { return this._name; }
	set name(v) { this._name = v; }

	/**
	 * @property {Array} items
	 */
	get items() { return this._items; }
	set items(v) {

		const a = [];

		let level = 0;

		if (v) {

			for (let i = v.length - 1; i >= 0; i--) {

				const it = v[i];
				if ( it === null || it === undefined ) continue;

				if (typeof it === 'object') level += it.level || 0;
				a.push(it);

			}


		}

		this.level = level;

		this._items = a;
	}


	/**
	 * @property {string} type - type might need to be a standard type
	 * in order to mimic a default item in item lists.
	 * 'custom' can distinguish as group.
	 */
	get type() { return this._type; }
	set type(v) { this._type = v; }

	/**
	 * @property {number} level
	 */
	get level() { return this._level; }
	set level(v) { this._level = v; }

	_cost = computed(()=>{

		const items = this.items;
		if (!items || items.length === 0) {
			return null;
		}
		const cost = {};

		for (let i = items.length - 1; i >= 0; i--) {

			const it = items[i];
			if (!it) items.splice(i, 1);
			else if (it.cost) addValues(cost, it.cost);


		}
		/*if (save) {
			this.effect = this.items.map(v => typeof v === 'string' ? v : v.name);
		}*/

		return cost;

	});

	_effect = computed(()=>{
		this.items.map(v => typeof v === 'string' ? v : v.name);
	});

	get effect(){
		return this._effect.value;
	}

	/// Cost to use group.
	get cost() {
		return this._cost.value;
	}
	set cost(v) {
		console.warn(`Attempt to set GROUP COST" ${v}`);
	}

	get locked() { return false; }
	get owned() { return true; }
	maxed() { return false; }

	constructor(vars = null) {

		if (vars) assign(this, vars);

		if (!this.items) this.items = [];

	}

	computeCost(save = true) {

		const items = this.items;
		if (!items || items.length === 0) {
			this.cost = null;
			return;
		}
		const cost = {};

		for (let i = items.length - 1; i >= 0; i--) {

			let it = items[i];
			if (!it) items.splice(i, 1);
			else if (it.cost) addValues(cost, it.cost);


		}

		if (save) {
			this.effect = items.map(v => typeof v === 'string' ? v : v.name);
			this.cost = cost;
		}
		return cost;

	}

	canUse(g) {

		//check for additional blocks like cd, locks, disabled, etc.
		for (let i = this.items.length - 1; i >= 0; i--) {
			if (!this.items[i].canUse(g)) return false;
		}

		return g.canPay(this.cost);

	}

	/**
	 *
	 * @param {} g
	 * @returns nothing
	 */
	onUse(g) {

		const len = this.items.length;
		for (let i = 0; i < len; i++) {

			this.items[i].onUse(g);

		}

	}

	add(it) {

		this.items.push(it);

	}

	/**
	 *
	 * @param {Game} g
	 * @param {*} amt
	 */
	amount(amt) {

		const len = this.items.length;
		for (let i = 0; i < len; i++) {
			this.items[i].amount(amt);
		}

	}

	/**
	 *
	 * @param {Object} mods - effect/mod description.
	 * @param {number} amt - factor of base amount added
	 * ( fractions of full amount due to tick time. )
	 */
	applyVars(mods, amt = 1) {

		const len = this.items.length;
		for (let i = 0; i < len; i++) {
			this.items[i].applyVars(mods, amt);
		}
	}

	/**
	 * Apply mod to every data of group.
	 * @param {Mod|Object} mods
	 * @param {number} amt
	 * @param {Object} [targ=null]
	 */
	applyMods(mods, amt = 1, targ, src, path, isMod = false, initialCall = true) {

		const len = this.items.length,
			results = [];

		for (let i = 0; i < len; i++) {
			const it = this.items[i];
			results.push(it.applyMods(mods, amt, it, isMod ? src : it, path ? path : it.id, isMod, initialCall));
		}
		return results;

	}

	/**
	 *
	 * @param {GameState} gs
	 */
	revive(gs) {

		this.items = gs.toData(this.items);
		//this.computeCost();

	}

}

mergeClass(Group, Base);