import {onMounted} from 'vue';
import Game from '../game';
import Debug from './debug';


let useCheats;

if ( !__CHEATS ) {

	useCheats = ()=>{};

} else {

	const cheatKeys = {
		b:'herbs',
		g:'gold',
		s:'scrolls',
		e:'exp',
		t:'stamina',
		h:"hp",
		k:'sp',
		m:'mana',
		c:'codices',
		a:'arcana',
		r:'research',
		w:'wizardhall'
	};

	let interval = 0;
	let enabled = false;
	let input = '';
	let debug;
	let state = Game.state;
	const enableCode = 'bodias';

	useCheats = ()=>{

		function cheatKey(e) {

			if (!interval) return;

			const active = document.activeElement;
			if ( active && active.tagName.toLowerCase() === 'input') return;

			const key = e.key.toLowerCase();
			if ( !enabled ) {
				testUnlock(key);
				return;
			}

			const targ = cheatKeys[key];

			if (key === 'p') {

				state.getData('runner').autoProgress();
				e.stopPropagation();

			} else if ( key ==='f') {

				debug.fillall();
				e.stopPropagation();

			} else if ( targ ) {
				if (e.shiftKey) state.addMax( targ );
				else {
					debug.doFill(targ);
				}
				e.stopPropagation();
			}

		}

		onMounted(() => {
			debug = window.debug || new Debug( Game );

			window.addEventListener('keydown', (e) => {
				if (e.repeat) return;
				cheatKey(e);
			}, false);

			enabled = false;
			input = '';
		});

		function testUnlock(key){

			input += key;
			if ( input === enableCode ) {

				console.warn('CHEATS ON');
				enabled = true;

			} else if ( input.length >= enableCode.length ) {
				input = input.slice( 1+ input.length - enableCode.length );
			}

		}
	}
}

export {useCheats}