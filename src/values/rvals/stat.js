import { computed, ref } from 'vue';
import RValue from './rvalue';
import Game from '../../game';
import { precise } from '../../util/format';
import { TYP_MOD, TYP_RVAL, TYP_STAT } from '../consts';


/**
 * Stat with a list of modifiers.
 */
export class Stat extends RValue {
	static getBase(s) {
		return s instanceof Stat ? s.base : s;
	}

	toJSON(){return super.value;}

	toString(){ return precise( this.value ); }

	/**
	 * @returns {number}
	 */
	valueOf() { return this._value.value; }

	set value(v){ super.value = v; }
	get value(){ return this._value.value; }

	/**
	 * @property {number} base
	 */
	get base() { return super.value; }
	set base(v) { super.value = v ?? 0; }

	_value = computed(()=>{

		const bTot = super.value + this._modBase.value;

		if ( this._pos === true ) {
	
			return Math.max( bTot + Math.abs(bTot)*(this._modPercent.value ),0);
	
		} else return bTot + Math.abs(bTot)*(this._modPercent.value);

	});

	_modBase = ref(0);
	_modPercent = ref(0);

	/**
	 * @property {number} bonus - total bonus to base, computed from mods.
	 * @protected
	 */
	get modBase(){return this._modBase.value; }

	/**
	 * @property {number} modPercent - mod pct bonuses, as decimal.
	 * Does not count implicit starting 1
	 * @protected
	 */
	get modPercent() { return this._modPercent.value };

	/**
	 * @property {number} prev - previous value of Stat calculated by recalc
	 */
	get prev() { return this._prev || 0; }
	set prev(v) { this._prev = v; }

	/**
	 * @property {object} - mods being applied by object
	 */
	get mod() { return this._mod; }
	set mod(v) { this._mod = v;	}

	/**
	 * @property {.<string,Mod>} mods - mods applied to object.
	 */
	get mods() { return this._mods; }
	set mods(v) {

		const mods = {};
		for( const p in v ) {

			const mod = v[p];
			mods[p] = (mod instanceof Mod ) ? mod : new Mod( v[p] );
		}
		this._mods = mods;
		this.recalc();
	}

	/**
	 * @property {boolean} pos - restrict stat to positive values after mods.
	 */
	get pos(){return this._pos; }
	set pos(v) { this._pos = v;}

	get type(){ return TYP_STAT }


	/**
	 *
	 * @param {Object|number} vars
	 * @param {string} path
	 */
	constructor( vars=null, path, pos ) {

		super( 0, path );

		if ( vars ) {

			if ( typeof vars === 'object') {

				if ( vars.type === TYP_RVAL ) {
					this.base = vars.value;
				} else {
					this.base = vars.base;
				}

			} else if ( typeof vars ==='number' ) super.value = vars;

		}

		if ( pos ) this.pos = pos;

		if ( !this.mod ) this.mod = {};
		if ( !this.mods ) this._mods = {};

	}

	/**
	 * @todo set modded value to match exactly?
	 * @param {number|Stat} v
	 */
	set(v) {
		super.value = typeof v === 'number' ? v : v.base;
	}

	/**
	 * Add amount to base stat.
	 * @param {number} amt
	 */
	add( amt ) {super.value += amt;}

	/**
	 * Apply a modifier to Stat, if the applied value can be a modifier.
	 * If not, the stat is permanently modified by the mod value.
	 * @param {Mod|number|Percent|Object} val
	 * @param {number} [amt=1]
	 */
	apply( val, amt=1 ) {

		if ( (val.type === TYP_MOD) && val.id ) return this.addMod( val, amt );
		
		if( val.type === TYP_MOD ) console.warn('MOD WITHOUT ID: ' + val );

		if ( val instanceof Stat ) val = val.valueOf();

		if ( typeof val ==='number' ) {

			this.base += amt*val;
			//deprec( this.id + ' mod: ' + mod );
			// console.warn( this.id + ' adding: ' + val +'  DEPRECATED NEW base: ' + this.vaTYP_MOD, lue );

			return;

		} else if ( typeof val === 'object') {

			/// when an object has no id, must apply to base.
			this.base += amt*( val.bonus || val.value || 0 );

			console.warn( this.id + ' DEPRECATED APPLY: ' + val + '  type: ' + val.constructor.name );

			//console.dir( val );

		} else {
			console.dir( val, 'unknown mod: ' + typeof val );
		}


	}

	/**
	 * Apply permanent modifier to stat.
	 * Used for instances.
	 * @param {Stat} mod
	 */
	perm( mod ) {

		console.warn( this.id + ' PERM MOD: ' + mod )
		if ( mod.countBonus ){
			this.base += mod.countBonus;
		} else if ( typeof mod === 'number') {
			this.base += mod;
		} else {

		}

	}

	/**
	 *
	 * @param {Mod} mod
	 * @param {number} amt
	 */
	addMod( mod, amt=1 ) {

		if ( !mod.id ) {
			console.dir( mod, 'NO MOD ID' );
			this.apply(mod, amt );

		} else {

			//should always occur (even at 0), to overwrite mods that have different values.
			this.mods[mod.id] = mod;
			this.recalc();

		}

	}

	/**
	 *
	 * @param {*} mod
	 */
	removeMods( mod ){

		const cur = this.mods[mod.id];
		if ( cur === undefined) return;

		this.mods[mod.id] = undefined;

		this.recalc();

	}

	/**
	 * Get the new stat value if base and percent are changed
	 * by the given amounts.
	 * @param {number} delBonus - delta base.
	 * @param {number} delPct - delta percent.
	 * @returns {number} - new stat value.
	 */
	delValue( delBonus=0, delPct=0 ) {
		return ( super.value + this.modBase + delBonus )*( 1 + this.modPercent + delPct );
	}

	/**
	 * Checks if the current value of this Stat has changed since last called.
	 * Calls applyMods if it has.
	 * @returns {boolean} if the value has updated
	 */
	update() {

		const current = this.valueOf();
		const result = current !== this.prev;

		if ( result && this.mod && Object.values(this.mod).length ) {
			Game.applyMods( this.mod, current );
		}

		this.prev = current;

		return result;

	}

	/**
	 * Recalculate the total bonus and percent applied to stat.
	 * @protected
	 */
	recalc(){

		let bonus = 0, pct = 0;

		for( const p in this._mods ) {

			const mod = this._mods[p];
			if (mod === undefined ) continue;

			pct += mod.countPct || 0;
			bonus += mod.countBonus || 0;

		}

		if ( pct != this._modPercent.value || bonus !== this._modBase.value){

			this._modPercent.value = pct;
			this._modBase.value = bonus;

			return this.update();
		}

	}

	canPay(amt) {
		const temp = (this.base - amt) + this._modBase.value;
		return !this.pos || temp*(1 + this._modPercent.value) >= 0;
	}

}