import Equip from './chars/equip';
import Inventory from './inventories/inventory';
import Minions from './inventories/minions';
import Combat from './composites/combat';
import Explore from './composites/explore';
import Group from './composites/group';
import Quickbars from './composites/quickbars';
import TagSet from './composites/tagset';
import DataList from './inventories/dataList';
import EnchantSlots from './inventories/enchantslots';
import SpellLoadouts from './inventories/spellLoadouts';
import UserSpells from './inventories/userSpells';
import Runner from './modules/runner';
import { ensure } from './util/util';
import { ARMOR, COMPANION, ENCHANTSLOTS, HOME, PURSUITS, TimeId, WEAPON, WEARABLE } from './values/consts';
import { Stat } from './values/rvals/stat';
import RValue from './values/rvals/rvalue';
import RevStat from './items/revStat';
import {Resource} from './items/resource';

export const REST_SLOT = 'rest';

/**
 * Used to avoid circular include references.
 * @param {string[]|object} list
 * @returns {DataList}
 */
export const MakeDataList = (list)=>{
	return new DataList(list);
}

export default class GameState {

	toJSON(){

		const slotIds = {};
		for( const p in this.slots ) {
			if ( this.slots[p] ) slotIds[p] = this.slots[p].id;
		}

		return {

			version:__VERSION,
			pid:this.pid,

			name:this.player.name,
			items:this.saveItems,
			bars:this.bars,
			slots:slotIds,
			equip:this.equip,
			combat:this.combat,
			drops:this.drops,
			explore:this.explore,
			sellRate:this.sellRate,
			currentSpellLoadout:this.currentSpellLoadout,
			NEXT_ID:this.NEXT_ID

		};

	}

	/**
	 * @property {string[]} modules - list of modules used.
	 */
	get modules(){return this._modules}
	set modules(v){this._modules = v;}

	/**
	 * Create unique string id.
	 * @param {string} [s='']
	 */
	nextId( s='' ) { return s + '_' + this.nextIdNum(); }

	nextIdNum() { return this.NEXT_ID++; }

	/**
	 *
	 * @param {Object} baseData - base game data.
	 */
	constructor( baseData ){

		Object.assign( this, baseData );

		/**
		 * @property {.<string,GData} saveItems - items actually saved.
		 * does not include hall items, or TagSets.
		 */
		this.saveItems = {};

		/**
		 * @property {Map<string,Inventory>} inventories - default inventories
		 * by item type, plus named inventories.
		 */
		this.inventories = new Map();

		/**
		 * @property {number} NEXT_ID - Next item id.
		 */
		this.NEXT_ID = this.NEXT_ID || 0;

		if ( !this.pid ) {

			/**@ hid compat */
			this.pid = this.player.hid || TimeId('p');
			if ( this.player.hid ) console.log('USING LEGACY HID: ' + this.player.hid );
			else console.log('GENERATING NEW PLAYERID: ' + this.pid );

		}

		this.initSlots();

		this.bars = new Quickbars(

			baseData.bars ||
				{ bars:[baseData.quickbar] }
		);

		// this.tagSets = baseData.items.tagSets;

		this.inventory = new Inventory( this.items.inv || baseData.inventory || {max:3} );
		this.items.inv = this.inventory;
		this.inventory.removeDupes = true;

		this.self = this.player;
		this.drops = new Inventory( baseData.drops );

		this.items[ENCHANTSLOTS] = new EnchantSlots( this.items[ENCHANTSLOTS] );

		/**
		 * @property {Minions} minions
		 */
		this.items.minions = this.minions = new Minions( baseData.items.minions || null );

		this.equip = new Equip( baseData.equip );

		this.initStats();

		this.combat = new Combat( baseData.combat );
		this.explore = new Explore( baseData.explore );

		this.runner = this.items.runner = new Runner( this.items.runner );

		this.prepItems();

		this.userSpells = this.items.userSpells = new UserSpells( this.items.userSpells );

		this.items.spelllist = this.spelllist = new DataList( this.items.spelllist );
		this.spelllist.spaceProp = 'level';
		this.spelllist.name = this.spelllist.id = 'spelllist';

		/**
		 * Support for Spell Loadouts (stored spell lists)
		 */

		this.items.spellLoadouts = this.spellLoadouts = new SpellLoadouts( this.items.spellLoadouts );

		/**
		 * Ensures we have a current Spell Loadout
		 * 
		 * If there is no currentSpellLoadout, it either means we're dealing with the first
		 * run under the spellLoadouts methodology, or that something else went wrong.
		 *
		 * Clone the spelllist out to the 0th list item if it does not exist.
		 * 
		 * Either way, set the currentSpellLoadout to the 0th list's id.
		 */

		if ( !this.currentSpellLoadout ){
			if ( this.items.spellLoadouts.items[0] === undefined ){
				this.spellLoadouts.create( this, "init", "Default Spell List");
				
				/**
				 * Then reinstantiate the data properly, because the game refuses
				 * to initialize this list properly as a Group object within the
				 * spellLoadouts container when called like this.
				 */
				const newid = this.spellLoadouts.items[0].id;

				this.spellLoadouts.items[0] = this.findData(newid);

			}

			this.currentSpellLoadout = this.spellLoadouts.items[0].id;

		}

		this.items.currentSpellLoadout = this.currentSpellLoadout;

		this.items.pursuits = new DataList( this.items.pursuits );
		this.items.pursuits.id = PURSUITS;

	}

	revive() {

		this.reviveItems();


		this.reviveSpecial();


		// quickbars must revive after inventory.
		this.bars.revive(this);

		// circular problem. spelllist has to be revived after created spells
		// compute their levels. unless levels stored in json?
		this.spelllist.calcUsed();

		/**
		 * @todo: messy bug fix. used to place player-specific resources on update-list.
		 * just move to player update()?
		 */
		/*
		let playerStats = this.player.getResources();
		if(this.playerStats.length !== playerStats.length && !this.playerStats.filter(it => !playerStats.includes(it)).length) {
			console.warn("Non-matching player stats", playerStats, this.playerStats);
			// this.playerStats = playerStats;
		}
		*/

		/**
		 * @property {Object.<string,TagSet>} tagsets - tag to array of items with tag.
		 * makes upgrading/referencing by tag easier.
		*/
		this.tagSets = this.makeTagSets( this.items, this.tagSets.reduce((obj, tagset) => {
			if(tagset.id) obj[tagset.id] = tagset;
			else console.warn("TAGSET MISSING ID: ", tagset);
			return obj;
		}, {}));

		this.items.allies = this.minions.allies;
		this.saveItems.allies = undefined;

	}

	/**
	 *
	 * @param {object} obj
	 * @param {(number)=>boolean} obj.tick -tick function.
	 */
	addTimer(obj){
		this.runner.addTimer(obj)
	}

	/**
	 * Game-wide stats.
	 */
	initStats() {

		/**
 		* @property {number} sellRate - percent of initial cost
 		* items sell for.
 		*/
		 this.sellRate = this.sellRate || new Stat(0.5, 'sellRate');

	}

	/**
	 * Revive custom items.
	 */
	prepItems() {

		for( const p in this.items ) {

			const it = this.items[p];

			if ( !it ) {
				console.warn('prepItems() item undefined: ' + p );
				delete this.items[p];
				continue;
			}
			/**
			 * special instanced item.
			 */
			if ( it.custom === 'group') {

				//console.warn('CUSTOM: ' + it.id + ' name: ' + it.name );
				this.items[p] = new Group( it );

			} else if ( it.instanced ) {

			}

		}


	}

	reviveSpecial() {

		for( let p in this.slots ) {
			if ( typeof this.slots[p] === 'string') this.slots[p] = this.getData(this.slots[p] );
		}
		this.restAction = this.slots[REST_SLOT];

		this.equip.revive( this );

		this.player.revive(this);

		this.minions.revive(this);
		this.drops.revive(this);

		this.combat.revive(this);
		this.explore.revive(this);
		new Set([this.player, ...this.minions.items, ...this.combat.enemies, ...this.combat.allies]).forEach(it => it.reviveDots(this))	
	}

	/**
	 * Check items for game-breaking inconsistencies and remove or fix
	 * bad item entries.
	 */
	reviveItems() {

		const manualRevive = new Set( ['minions', 'player', 'explore', 'equip', 'drops'] );

		let count = 0;
		for( const p in this.items ) {

			const it = this.items[p];
			/**
			 * revive() has to be called after prepItems() so custom items are instanced
			 * and can be referenced.
			 */
			if ( it.revive && typeof it.revive === 'function' && !manualRevive.has(p) ) {
				it.revive(this);
			}

			if ( !it.hasTag ) {

				console.warn( p + ': ' + this.items[p].id + ' missing hasTag(). Removing.');
				delete this.items[p];

			} else {

				this.saveItems[p] = it;
				// need hasTag() func.
				if ( it.hasTag(HOME)) {
					it.need = this.homeTest;
				}
				count++;
			}

		}
		console.log('Items Total: ' + count);

	}

	/**
	 * Test if a home can fit the current used capacity.
	 * @param {Object.<string,Items>} g - all game data.
	 * @param {GData} i - item being tested.
	 * @param {GameState} gs
	 */
	homeTest( g, i, gs ) {

		const cur = gs.slots.home;

		return g.space.valueOf()<=
			g.space.max.delValue( i.mod.space.max.bonus - ( cur ? cur.mod.space.max.bonus : 0) );

	}


	initSlots(){

		/**
		 * @property {Object.<string,Item>} slots - slots for items which can only have
		 * a single active at a given time.
		 */
		this.slots = this.slots || {};

		// must be defined for Vue. slots could be missing from save.
		ensure( this.slots, [HOME, 'mount', 'bed', REST_SLOT, COMPANION ]);
		if ( !this.slots[REST_SLOT] ) this.slots[REST_SLOT] = this.getData('rest');

	}

		/**
	 * Tests if a task has effectively filled a resource.
	 * @param {string|string[]} v - data or datas to fill.
	 * @param {GData} a - task doing the filling.
	 * @param {string} - name of relavant filling effect ( for tag-item fills)
	 */
	filled( v, a, tag ) {

			if ( Array.isArray(v) ) {
				for( let i = v.length-1; i>=0; i-- ) {
					if ( !this.filled( v[i], a, tag ) ) return false;
				}
				return true;
			}
	
			const item = this.getData(v);
			if (!item) {
				console.warn('missing fill item: ' + v );
				return true;
			}
			if ( !item.rate || !a.effect || item.rate >= 0 ) return item.maxed();
	
			// actual filling rate.
			tag = a.effect[ tag || v ];
	
			return ( !tag || item.filled(tag ) );
	
		}

	/**
	 * Test if mods can be safely applied.
	 * @param {object} mod
	 * @returns {boolean}
	 */
	canMod( mod, src ) {

		if ( typeof mod !== 'object') return true;
		if (Array.isArray(mod) ) return mod.every( v=>this.canMod(v), this );

		for( const p in mod ) {

			const sub = mod[p];

			if ( !isNaN(sub) || sub instanceof RValue ) {

				const res = this.getData(p);

				if ( res instanceof RevStat ) {

					return ( res.canPay(sub) );

				} else if ( res instanceof Resource ) return ( res.canPay(-sub) );
			}

		}

		return true;

	}

	/**
	 * Attempts to pay the cost to perform an task, buy an upgrade, etc.
	 * Before calling this function, ensure cost can be met with canPay()
	 *
	 * @param {Array|Object} cost
	 */
	payCost( cost, unit=1) {

		if ( cost === undefined || cost === null ) return true;
		if ( Array.isArray(cost)  ) return cost.forEach( v=>this.payCost(v,unit), this );

		if ( typeof cost === 'object' ){

			for( const p in cost ) {

				const res = this.getData(p);

				if ( !res || res.instanced || res.isRecipe ) {
					this.payInst( p, cost[p]*unit );

				} else {

					const price = cost[p];

					if ( !isNaN(price) ) this.remove( res, price*unit );
					else if ( typeof price === 'object' ) {

						res.applyVars( price, -unit );

					} else if ( typeof price === 'function') {
						this.remove( res, unit*price(this.gdata, this.player) )
					}

				}


			}

		}

	}

		/**
	 * Remove amount of a non-inventory item.
	 * If a tag list is specified, the amount will only be removed
	 * from a single element of the list. Apparently.
	 * @property {string|GData} id - item id or tag.
	 */
	remove( id, amt=1 ){

		const it = typeof id === 'string' ? this.getData(id) : id;
		if ( !it ) return;
	
		if ( it.slot ) { if ( this.getSlot(it.slot) === it ) this.setSlot(it.slot, null); }
	
		it.remove(amt);
	
		if ( it.mod ) this.applyMods( it.mod, -amt );
		if ( it.lock ) this.unlock( it.lock, amt );

	
	}

		/**
	 * Decrement lock count on an Item or array of items, etc.
	 * @param {string|string[]|GData|GData[]} id
	 */
		unlock( id ) { this.lock(id, -1); }

		/**
	 * Increment lock counter on item or items.
	 * @param {string|string[]|GData|GData[]} id
	 */
	lock(id, amt=1 ) {

		if ( typeof id === 'object' && id[Symbol.iterator] ) {

			for( let v of id ) this.lock(v,amt);

		} else if ( typeof id === 'object' ) {

			id.locks += amt;

		} else {

			const it = this.getData(id);
			if ( it ) {
				this.lock(it, amt);
			}

		}


	}

	/**
	 * Apply a mod.
	 * @param {Array|Object|string} mod
	 * @param {number} amt - amount added.
	 */
	applyMods( mod, amt=1 ) {

		if ( !mod ) return;

		if ( Array.isArray(mod)  ) {
			for( const m of mod ) this.applyMods(m, amt);
		} else if ( typeof mod === 'object' ) {

			for( const p in mod ) {

				const target = this.getData( p );
				if ( target === undefined || target === null ) {

					//if this is reached, the json files should be inspected, using p and mod as a guide
					console.warn( "game.applyMods SKIPPED TARGET:", p, mod );

				} else if ( mod[p] === true ){

					target.doUnlock(this);

				} else {

					if ( target.applyMods) {

						target.applyMods( mod[p], amt );

					} else console.warn( 'no applyMods(): ' + target );

				}
			}

		} else if ( typeof mod === 'string') {

			const t = this.getData(mod);
			if ( t ) {

				console.warn('!!!!!!!!!!ADDED NUMBER MOD: ' + mod );
				t.amount( 1 );

			}

		}

	}

	payInst( p, amt ){

		const res = this.inventory.find( p,true );
		if ( res ) this.inventory.removeCount(res,amt);

	}

	/**
	 * Return the results of a testing object.
	 * @param {string|function|Object|Array} test - test object.
	 * @param {?GData} [item=null] - item being used/unlocked.
	 * @returns {boolean}
	 */
	unlockTest( test, item=null ) {

		if ( test === null || test === undefined ) {
			console.warn('test not found: ' + test + ' : ' + item );
			return true;
		}
		//console.log('trying unlock: ' + item.id );
		const type = typeof test;
		if ( type === 'function') {
			return test( this.items, item, this );
		}

		else if ( type === 'string') {

			// test that another item is unlocked.
			const it = this.getData(test);
			return it && it.fillsRequire(this);

		} else if (  Array.isArray(test) ) {

			for( let i = test.length-1; i >= 0; i-- )
				if ( !this.unlockTest(test[i], item ) ) return false;
			return true;

		} else if ( type === 'object' ) {

			/**
			 * @todo: quick patch in case it was a data item.
			 */
			if ( test.id ) return test.fillsRequire(this);

			// @todo: take recursive values into account.
			// @todo allow tag tests.
			for( const p in test ) {

				const it = this.getData(p);
				if ( it && it.value < test[p] ) return false;

			}
			return true;

		}

	}

		/**
		 * Determines whether an item can be run as a continuous task.
		 * @returns {boolean}
		 */
		canRun( it ) {

			if ( !it.canRun ) {
				console.error( it.id + ' no canRun()');
				return false;
			} else return it.canRun( this, TICK_LEN );
	
		}

	/**
	 * Determine if an object cost can be paid before the pay attempt
	 * is actually made.
	 * @param {Array|Object} cost
	 * @returns {boolean} true if cost can be paid.
	 */
	canPay( cost, amt=1 ) {

		// @note @todo: this doesnt work since some items might charge same cost.
		if (Array.isArray(cost) ) return cost.every( v=>this.canPay(v,amt), this );

		if ( typeof cost === 'object' ){

			for( const p in cost ) {

				const sub = cost[p];
				const res = this.getData(p);

				if ( !res ) return false;
				else if ( res.instanced || res.isRecipe ) {

					/* @todo: ensure correct inventory used. map type-> default inventory? */
					if(!this.inventory.hasCount( res, amt )) return false;

				} else if ( !isNaN(sub) || sub.isRVal ) {

					if ( !res.canPay ) console.warn("Missing canPay function for", res);
					else if ( !res.canPay(sub*amt) ) return false;
					//if ( res.value < sub*amt ) return false;

				} else {

					// things like research.max. with suboject costs.
					if ( !res.canPay ) console.warn("Missing canPay function for", res);
					else if ( !this.canPayObj( res, sub, amt ) ) return false;

				}

			}

	}

	return true;
}

/**
 * Follow object path to determine ability to pay.
 * @param {object} parent - parent object.
 * @param {object|number} cost - cost expected on parent or sub.
 * @param {number} amt - cost multiplier.
 * @returns {boolean}
 */
canPayObj( parent, cost, amt=1 ){

	if ( !parent ) return false;

	if ( cost instanceof Function ) {
		cost = cost(this.items, this.player); //TODO does not account updated costs.
	}

	if ( (cost instanceof RValue) || !isNaN(cost)){
		return parent.canPay ? parent.canPay(cost * amt) : parent >= cost * amt;
	}

	if( typeof cost !== "object" && isNaN(cost) ) console.warn("canPayObj cost is non-object NaN", cost);

	for( const p in cost ) {

		const val = cost[p];
		if ( !isNaN(val) || (val instanceof RValue) ) {
			if ( parent.canPay ) {
				if ( !parent.canPay(val*amt) ) return false;
			} else if ( parent.value < val*amt ) return false;
		} else if ( typeof val === 'object'){


			//console.log('checking sub cost: ' + p + ' ' +cost.constructor.name );
			//if ( parent ) console.log( 'parent: ' + parent.id );

			if ( !this.canPayObj( parent[p], val, amt ) ) return false;
		}

	}

	return true;
}

	/**
	 * Create lists of tagged items.
	 * @param {.<string,GData>} items
	 * @returns {Object.<string,GData[]>} lists
	 */
	makeTagSets( items, tagSets = {} ) {

		for( const p in items ) {

			const it = items[p];
			const tags = it.tags;
			if ( !tags ) continue;

			for( const t of tags ){

				let list = tagSets[t];
				if ( !list ) {
					items[t] = tagSets[t] = list = new TagSet(t);
				}

				list.add( it );

			}

		}

		return tagSets;

	}

	/**
	 *
	 * @param {GData} it
	 * @param {number} slotNum
	 */
	setQuickSlot( it, slotNum ) {
		this.bars.active.setSlot(it, slotNum);
	}

	/**
	 * Get quickslot item for slot number.
	 * @param {number} slotNum
	 * @returns {?GData}
	*/
	getQuickSlot( slotNum ) {
		return this.bars.active.getSlot( slotNum);
	}

	/**
	 * Replace all ids in array with corresponding GData.
	 * @param {Array.<string|GData>} a
	 * @returns - the original array.
	 */
	toData(a) {

		if (!a) return [];

		for( let i = a.length-1; i >= 0; i-- ) {

			if ( typeof a[i] === 'string') a[i] = this.getData(a[i]);

		}

		return a;
	}

	/**
	 *
	 * @param {string} tag
	 * @returns {GData[]|undefined}
	 */
	getTagSet( tag ) {
		return this.tagSets[tag];
	}

	/**
	 * Get the cost of a given subtype to buy item.
	 * @param {string} type
	 * @returns {number}
	 */
	typeCost( cost, type ) {
		return cost?.[type] ?? 0;
	}

	/**
	 * Add to maximum value of resource.
	 * Used for cheat codes.
	 * @param {string} id
	 * @param {number} amt
	 */
	addMax( id, amt=50) {

		const it = typeof id === 'string' ? this.getData(id) : id;
		if ( !it) return;

		it.max.base += amt;
	}

	/**
	 *
	 * @param {(it)=>boolean} pred
	 */
	filterItems( pred ) {
		const a = [];
		const items = this.items;
		for( const p in items ) {
			if ( pred( items[p] ) ) a.push( items[p] );
		}
		return a;
	}

	/**
	 * Add created item to items list.
	 * @param {GData} it
	 */
	addItem( it ) {

		if ( this.items[it.id] ) console.warn('OVERWRITE ID: ' + it.id);

		if ( !it.hasTag ) {
			console.log('MISSING HASTAG: ' + it.id );
			return false;
		}

		this.items[it.id] = it;

		if ( it.module !== 'hall') {
			this.saveItems[it.id] = it;
		}

		return true;

	}

	/**
	 * Should only be used for custom items.
	 * Call from Game so DELETE_ITEM event called.
	 * @param {GData} it
	 */
	deleteItem( it ) {
		delete this.items[it.id];
		delete this.saveItems[it.id];
	}

	/**
	 * Get state slots so they can be modified for Vue reactivity.
	 * @returns {.<string,GData>}
	 */
	getSlots(){ return this.slots; }

	/**
	 * Get item in named slot.
	 * @param {string} id - slot id.
	 * @param {string} type - item type for determining subslot (equip,home,etc)
	 * @returns {?GData}
	 */
	getSlot( id, type) {
		if ( type === WEARABLE || type === ARMOR || type ===WEAPON ) return null;
		return this.slots[id];
	}

	/**
	 * Set slotted item for exclusive items.
	 * @param {string} slot
	 * @param {?GData} v - item to place in slot, or null.
	 */
	setSlot(slot,v) {

		if ( v && (v.type === WEARABLE) ) return;
		this.slots[slot] = v;

		if ( slot === REST_SLOT ) this.restAction = v;

	}

	/**
	 * Find an item instantiated from given item proto/recipe.
	 * @param {string} id
	 */
	findInstance( id ) {
		return this.inventory.find(id, true) || this.equip.find(id, true );
	}

	exists(id){ return this.items.hasOwnProperty(id);}

	/**
	 * Find item in base items, equip, or inventory.
	 * @param {string} id
	 * @param {boolean} [any=false] - whether to return any matching instanced item.
	 */
	findData(id, any=false) {

		return this.getData(id) || this.inventory.find(id, any) || this.equip.find(id, any );
	}

	/**
	 * Check if an item is unique and already exists, or been
	 * instanced.
	 * @param {string|GData} it
	 */
	hasUnique(it) {

		if ( typeof it ==='string') it = this.getData(it);

		if ( it === undefined || !it.unique ) return false;

		if ( it.isRecipe || it.instanced ) {

			return this.inventory.find(it.id,true) != null ||
			this.drops.find(it.id,true) != null || this.equip.find(it.id,true) != null || this.items.enchantslots.findItem(it.id,true) != null;

		} else return it.value > 0;

	}

	/**
	 * Return item, excluding uniques with value > 0.
	 * @param {string} id
	 */
	getUnique(id) {

		let it = this.items[id];
		return ( it === undefined || !it.unique ) ? it : (
			it.value>0 ? null : it
		);

	}

	getData(id) { return this.items[id] || this[id]; }

	getMonster(v) {return this.combat.enemies.find(w => w.id === v) || this.combat.allies.find(w => w.id === v) || this.minions.items.find(w => w.id === v)}

	unlockFunc(...unlockers) { 
		let totalunlock = 0
		for (let i of unlockers){
			
			totalunlock += i*(i.unlockweight||0)
		}
		return totalunlock||0
	}
}