import { shallowReactive } from "vue";
import Game from '../game';


///  Information about current rollover object.
/*type RollOver = {
    elm: HTMLElement | null,
    item: string[] | Item | Base | GData | null,
    title: string | null,
    source?: string | object | null,
    context?: object | null
}*/
/// todo: this might need to be function.
const createPopupStore = () => {

    const rollOver = shallowReactive({
        item: null,
        elm: null,
        title: null,
        text:null,
        source: null
    });

    /**
     * Call on item rollover.
     * @param {MouseEvent} evt 
     * @param {string[]|Item|GData|null} it 
     * @param {object} source 
     * @param {string|null} title 
     */
    function itemOver(evt,
        it=null,
        source=null,
        title= null,
        text=null) {

        rollOver.item = it ?? null;
        rollOver.elm = evt.currentTarget ?? null;
        rollOver.source = source;

        rollOver.context = source?.context ?? Game;
        rollOver.text = text ?? null;

        rollOver.title = title;
    }

    /// call on item roll out.
    function itemOut() {
        rollOver.item = null;
        rollOver.elm = null;
        rollOver.source = null;
        rollOver.title = null;
        rollOver.context = null;
        rollOver.text = null;
    }

    return {

        rollOver,
        itemOver,
        itemOut

    };

}

/**
 * TODO: should be able to remove this store by associating data with HTMLElements
 * and automatically listening for roll over events.
 */
let popupStore = null;

export const useRollOver = () => {
    return popupStore ?? ( popupStore = createPopupStore());
}