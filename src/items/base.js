import Game from '../game';
import { ParseMods } from '../modules/parsing';
import { Changed } from '../techTree';
import { changes, cloneClass, getDescs, jsonify } from '../util/objecty';
import { canWriteProp, splitKeys, subset } from '../util/util';
import { DESCENDLIST, TYP_MOD, UNTAG } from '../values/consts';
import {Mod} from '../values/mods/mod';
import FValue from '../values/rvals/fvalue';
import RValue from '../values/rvals/rvalue';
import {Stat} from '../values/rvals/stat';

export const SetModCounts = ( m, v)=>{

	if ( m instanceof Mod ) {
		//console.log('setting mod count: ' + m.id + ' val: ' + v );
		m.count = v;
	}
	else if ( typeof m ==='object') {
		for( let p in m ){ SetModCounts(m[p], v); }
	}

}


export const mergeClass = ( destClass, src ) => {

	let proto = destClass.prototype || destClass;
	let srcDescs = Object.getOwnPropertyDescriptors( src );
	let protoDescs = getDescs(proto);

	// NOTE: valueOf not overwritten.
	for( let p in srcDescs ) {
		if ( !protoDescs.has(p) ) Object.defineProperty( proto, p, srcDescs[p]);
	}

	return destClass;

}

const ModKeys = ["runmod", "mod"];

//NOTE if there is a circular reference within the object, this WILL infinitely loop
/**
 * Collects the first encountered mod property within each of an object's properties into one array.
 * @param {Object} obj object to be reduced
 * @returns {Array<Object>} objects array containing all mod property values found
 */
function findMods( obj, src ) {
	//prevents delving into any instances of classes; should only be dealing with Objects
	if( !obj || !( typeof obj === "object" && obj.constructor === Object ) ) return {};

	//Grabbing mods within current object
	let [mods, check] = subset(obj, ...ModKeys);
	ModKeys.forEach(modtype => {
		mods[modtype] = mods[modtype] ? [mods[modtype]] : [];
	});

	//Collects all mods into one object
	for(let key in check) {
		let res = findMods(check[key]);
		ModKeys.forEach(modtype => {
			if(res[modtype]) mods[modtype].push(...res[modtype])
		});
	}
	return mods;
}


 /**
  * @todo shorten list by implementing better base/defaults logic.
  * @const {Set.<string>} JSONIgnore - ignore these properties by default when saving.
  */
 const JSONIgnore = new Set( ['template', 'id', 'type', 'defaults', 'module', 'sname',
	 'sym', 'warn', "effect", "length", 'runmod', 'name', 'desc', 'running', 'current', 'warnMsg',
	 'once', 'context', 'enemies', 'encs', 'boss', 'spawns','targets','only',
	 'locked', 'locks', 'value', 'exp', 'delta', 'tags', 'mod', 'alter', 'progress','need', 'require','action', 'repeat' ]);

/**
 * Base class of all Game Objects.
 */
export default {

	toJSON() {

		if ( this.save && (this.value>0||this.owned)) return this.forceSave();

		//the JSON parse + stringify is to make sure that all items in vars have been formatted to JSON.
		//jsonify only does shallow toJSON, and won't toJSON inner objects.
		let vars = JSON.parse(JSON.stringify(jsonify(this, JSONIgnore ) || {}));
		if ( this.template ) {
			vars = changes( vars, this.template );
			if(this.cost && this.template.cost != null) {
				if(this.template.cost instanceof String) delete vars.cost;
				else if(typeof this.template.cost === "number" && vars.cost.gold === this.template.cost) delete vars.cost;
				else if(vars?.cost) {
					let ref = cloneClass(this.template.cost);
					splitKeys(ref);
					if(!changes(vars.cost, ref)) delete vars.cost;
				}
			}
		}

		let defaults = this.Defaults || this.constructor.Defaults;
		if(defaults) {
			// Warning: changes doesn't do strict equality checks, which means falsy values can be lost.
			// vars = changes(vars, defaults);
			for(let key in defaults) {
				if(vars[key] === undefined) continue;
				if(!(defaults[key] instanceof Object)) {
					if(vars[key] === defaults[key]) {
						delete vars[key];
					}
				} else {
					//TODO nested object defaults
				}
			}
		}

		if ( this.locked === false && this.template && this.template.locked !== false ){
			vars = vars || {};
			vars.locked = this.locked;
		}
		if ( vars && vars.name ) vars.name = this.sname;

		if (vars && this.timer > 0) vars.timer = this.timer;

		return vars && Object.keys(vars).length ? vars : undefined;

	},

	forceSave(){

		let data = jsonify(this);
		if ( this.mod ) data.mod = this.mod;
		if ( this.slot ) data.slot = this.slot;
		if ( this.effect) data.effect = this.effect;
		if ( this.use ) data.use = this.use;

		if ( data.template && typeof data.template === 'object' ) data.template = data.template.id;
		if ( data.val ) data.value = undefined;
		data.name = this.sname;

		return data;

	},

	/**
	 * @property {string} id
	 */
	get id() { return this._id; },
	set id(v) { this._id = v;},

	/**
	 * @property {Object} template - original data used to create the Item.
	 * Should be raw, immutable data.
	 */
	get template() { return this._template; },
	set template(v) { this._template = v;},

	/**
	 * @property {string} type
	 */
	get type() { return this._type },
	set type(v) { this._type =v;},

	get typeName(){return this._type;},

	/**
	 * @property {string} id - internal id.
	 */
	toString(){return this.id;},

	/**
	 * @property {string} sname - Simple name without symbol.
	 */
	get sname(){ return this._name || this.id; },

	/**
	 * @property {string} name - displayed name.
	 */
	get name() { return (( this._actname && this._value.value < 1 ) ? this.actname
		: (this.sym||'') + (this._name||this.id));
	},
	set name(v) {

		if ( v&&this.sym ) {

			this._name = v.split(this.sym).join( '').trim();

		} else this._name = v;


	},

	/**
	 * @property {string} sym - optional unicode symbol.
	 */
	 get sym() { return this._sym; },
	 set sym(v) { this._sym=v;},

	/**
	 * @property {boolean} repeat - whether the item is repeatable.
	 */
	get repeat(){return this._repeat;},
	set repeat(v) { this._repeat=v},

	/**
	 * @property {string} desc
	 */
	get desc() { return ( this.actdesc && this._value < 1 ) ? this.actdesc : (this._desc || null ); },
	set desc(v) { this._desc=v;},

	/**
	 * @property {number} current - displayable value; override in subclass for auto rounding, floor, etc.
	 */
	get current() { return this.value },

	/**
	 * @property {number} ex - save/load alias for exp without triggers.
	 */
	get ex(){return this._exp; },
	set ex(v) { this._exp = v;},

	/**
	 * @property {number} val - saving/loading from json without triggers.
	 */
	get val() { return this._value },
	set val(v) { this._value = v; },

	/**
	 * @property {number} value
	 */
	get value() { return this._value; },
	set value(v) {
		this._value = v;
	},
	valueOf() { return this._value; },

	/**
	 * @property {number} delta - value change this frame.
	 * added to value at end of frame.
	 */
	get delta(){return 0;},

	/**
	 * @property {string[]} tags - tag to distinguish between
	 * item subtypes.
	 */
	get tags() { return this._tags;},
	set tags(v) {

		if ( typeof v === 'string') {
			this._tags = v.split(',');
		} else if ( Array.isArray(v) ) {

			this._tags = v;

		} else this._tags = null;

	},

	permVars( mods, targ=this ) {

		if ( typeof targ === 'number') {

			// error.

		} else if ( typeof mods === 'object') {

			for( const p in mods ) {

				const mod = mods[p];
				let sub = targ[p];

				if ( sub === undefined || sub === null ) {

					sub = targ[p] = cloneClass( mod );

				} else if ( typeof sub === 'number' ) {

					targ[p] = (sub||0) + mods[p].valueOf();

				} else if ( typeof mod === 'object' ) {

					if ( mod.constructor !== Object ) sub.perm( mod );
					else this.permVars( mod, sub );

				}
				else console.log( this.id + ' UNKNOWN PERM VAR: ' + p + ' typ: ' + (typeof sub ));


			}

			if ( targ === this && mods.mod ) {
				ParseMods( this.mod, this.id, this );
				//SetModIds( targ.mod, targ.id );
			}

		}

	},

	/**
	 *
	 * @param {Object} vars - values to change/add.
	 * @param {number} amt - factor of base amount added
	 * ( fractions of full amount due to tick time. )
	 */
	applyVars( vars, amt=1 ) {

		if ( typeof vars === 'number') {

			//deprec( this.id + ' mod: ' + mods );
			this.value.add( vars*amt );

		} else if ( vars.isRVal ) {

			this.amount( amt*vars.getApply( Game.state, this ) );
			//this.value.add( amt*vars.getApply( Game.state, this ) );


		} else if ( typeof vars === 'object' ) {

			if ( vars.mod ) this.changeMod( vars.mod, amt );

			for( let p in vars ) {

				// add any final value last.
				if (  p === 'skipLocked' || p === 'value') continue;

				let targ = this[p];
				let sub = vars[p];

				

				if ( sub instanceof Function ) sub = sub( Game.gdata );

				if ( sub instanceof FValue ) sub = sub.fn( Game.gdata );

				if ( targ instanceof RValue ) {

					targ.apply( sub, amt );

				} else if ( typeof sub === 'object' ) {

					if ( sub.type === TYP_MOD ) {

						sub.applyTo( this, p, amt );

					} else if ( typeof targ === 'number' || sub.isRVal ) {

						//deprec( this.id + ' targ: ' + p + ': ' + targ );
						this[p] += Number(sub)*amt;
					} else {
						this.subeffect( targ, sub, amt );
					}

				} else if ( targ !== undefined ) {

					this[p] += Number(sub)*amt;

				} else {
					console.log('NEW SUB: ' + p );
					this.newSub( this, p, sub, amt )
				}

			}
			if ( vars.value ) {
				this.amount( vars.value*amt);
				//this.value += (vars.value)*amt;
			}

		}


		Changed.add(this);

	},

	/**
	  * Apply mod(s) recursively.
	 * @param {Object} mods - Mods being applied to target
	 * @param {number} [amt=1] - mod multiplier. Used when a mod doesn't have an id.
	 * @param {Object} [targ=this] - target of mods.
	 * @param {Object} [src=this] - source of mods. Used for applyObj 
	 * @param {string} [path=targ.id] - Path used for creating ids.
	 * @param {string} [modType=null] - subobject mod check.
	 * @returns {Object} mod path object used in applyObj
	 */
	applyMods( mods, amt=1, targ=this, src=this, path=this.id, modType=null, initialCall=true ) {

		Changed.add(this);

		if ( mods instanceof Mod ) {

			return mods.applyTo( targ, 'value', amt );

		} else if ( mods.constructor === Object ) {

			let nextMods = this.applyObj( mods, amt, targ, src, path, modType );
			if ( initialCall ) {
				let { mod, runmod } = findMods( nextMods );
				this.changeMod( mod , amt );
				if ( this.running ) this.changeMod( runmod, 1, false );
			}

			return nextMods;

		} else if ( typeof mods === 'number') {

			console.warn( mods + ' RAW NUM MOD ON: ' + this.id );

			/*if ( targ instanceof Stat ) {

				console.error( '!!!!! ' + mods + ' number apply to: ' + this.id );
				targ.add( mods*amt );

			} else if ( typeof targ === 'object') {

				console.warn( this.id + ' TARG is RAW OBJECT: ' + mods );
				targ.value = (targ.value || 0 ) + amt*mods;

			} else {
				// nothing can be done if targ is a number. no parent object.
				console.error( this.id + ' !!invalid mod: ' + mods );
			}*/

		} else console.warn( this.id + ' unknown mod type: ' + mods );

		return null;

	},

	/**
	 * Apply a mod when the mod is recursive object.
	 * @param {Object} mods - Mods being applied to target
	 * @param {number} amt - mod multiplier. Used when a mod doesn't have an id.
	 * @param {Object} targ - target of mods.
	 * @param {Object} [src=this] - source of mods. Used for new stats/mods.
	 * @param {string} path - Path used for creating ids.
	 * @param {?string} [modType=null] - subobject mod check.
	 * @returns {Object} New mod objects
	 */
	applyObj( mods, amt, targ, src, path, modType ) {

		let results = {}

		for( let p in mods ) {

			let res = null;

			let subMod = mods[p];
			let subTarg = targ[p];
			let modCheck = modType || (ModKeys.includes(p) ? p : null);

			//Only needed when subTarg is null or undefined
			let newSrc = modCheck || isNaN(+subTarg) ? src : subTarg; //may need a better check for source.
			let newPath = (path ? path + '.' : '') + p;

			// Define a list of targeted objects that require special handling
//			let descendArr = ["cost","run","effect","result","convert","input","output"];

			if ( (subTarg === undefined || subTarg === null) ) {

				if( !canWriteProp(targ, p ) ) continue;

				if ( subMod.constructor === Object && (modCheck || !p.startsWith(UNTAG))) {

					res = this.applyObj( subMod, amt, targ[p]={}, newSrc, newPath, modCheck );

				} else if ( p.startsWith(UNTAG) && !modCheck ) {
					//console.warn("Un-tagging: ", subMod, targ, p, amt);
					if(subMod?.applyTo) {
					/**
					 * Basic scenario, such as thing.untag_stress
					 * We send directly to applyTo to handle the untagging
					 */
						res = subMod.applyTo( targ, p, amt );
					} else {
					/**
					 * Sub-target scenario - thing.untag_skill.exp
					 * For each child key under the untag, we split things off
					 * NOTE - this will probably only work 1 level down at the moment, 
					 * but that should be sufficient for the current use cases
					 */
						res = [];
						for(const key in subMod){
							const childMod = subMod[key];
							if(childMod?.applyTo) {
								const retarg = childMod.applyTo(targ, p, amt, false, key);
								res.unshift(retarg);
							}
						}
						continue;
					}

				} else if( DESCENDLIST.includes(p) && modCheck === null ){
					/**
					 * If we are trying to mod a non-specific member of the DESCENDLIST list, but
					 * if doesn't already exist (eg. .cost on something with no cost), just skip it.
					 */
					//console.warn(mods, p, subMod, targ, src, path, modType, modCheck);
					continue;

				} else {
					// This is the entry point when multiple items under a DESCENDLIST member could exist
					// When that happens, we need to handle .value and all other members separately
					let descVal = false;
					if(p === "value") {
						let srcId = src.id;
						for (let d in DESCENDLIST) {
							let descendStr = DESCENDLIST[d];
							let checkPath = srcId + "." + descendStr + ".value";
							if (newPath === checkPath){
								descVal = true;
								res = subMod.applyTo( src, descendStr, amt );
//								console.warn(subMod, src, descendStr, amt);
								continue;
							}
						}
					}
					if (descVal) continue;

					let params = [
						0, //vars
						newPath, //id
						modType !== "runmod" ? newSrc : 1 //source
					];

					(res = targ[p] = modCheck ? new Mod( ...params ) : new Stat( ...params )).addMod( subMod, amt );

										//@todo use more accurate subpath.
					// subTarg.id = SubPath(this.id, p );
					//console.log( this.id + '.' + p  + ': ' + subMod + ': targ null: ' + subTarg.valueOf() + ' mod? ' + isMod );
				}


			} else if ( subTarg.applyMods ) {

				res = subTarg.applyMods( subMod, amt, subTarg, newSrc, newPath, modCheck );

			} else if ( subMod.constructor === Object ) {

				res = this.applyObj( subMod, amt, subTarg, newSrc, newPath, modCheck );

			} else if ( subMod instanceof Mod ) {

				res = subTarg.isRVal ? subTarg.addMod( subMod, amt ) : subMod.applyTo( targ, p, amt );
				/*if( DESCENDLIST.includes(p) && modCheck === null ){
					console.warn("Descending with applyTo: ", subMod, targ, p, amt);
				}*/
				
			}else if ( typeof subMod === 'number' ) {
				console.warn( 'RAW NUMBER MOD on: ' + this.id + ': ' + p + ': ' + subMod );
			}
			else if ( typeof subMod === 'boolean' ) {
				if (amt>0) {
					subTarg.value = subMod;
				} else if (amt<0) {
					subTarg.value = !subMod;
				};
			}
			/*else if ( typeof subMod === 'number' ) {

				if ( typeof subTarg === 'number') {

					console.warn('MOD OF RAW NUM: ' + p + ' : ' + (m*amt ) );
					targ[p] = new Stat( targ[p] + subMod*amt, SubPath(this.id, p) );

				} else this.applyMods( subMod, amt, subTarg);

			}*/else {

				console.warn( `UNKNOWN Mod to ${this.id}.${p}: ${subMod}` + '  ' + typeof subMod);
			}

			if ( res && Object.keys(res).length ) results[p] = res;

		}

		return results;

	},

	/**
	 *
	 * @param {Mod|Object} mods
	 * @param {Object} [targ=null]
	 */
	removeMods( mods, targ=this ) {

		if ( targ === this ) Changed.add(this);
		else if ( !targ ) return;

		if ( mods instanceof Mod ) {

			if ( typeof targ === 'object') {

				if ( targ.isRVal ) targ.removeMods( mods );
				else this.removeMods(mods, targ.value );
			}

		} else if ( mods.constructor === Object ) {

			for( let p in mods ) {
				this.removeMods( mods[p], targ[p] );
			}

		} else if ( typeof mods === 'number') {

			console.warn( this.id + ' REMOVED NUMBER MOD: ' + mods );

		} else console.warn( this.id + ' unknown mod type: ' + mods );

	},

	/**
	 * Perform a subobject assignment.
	 * @param {Object} obj - base object being assigned to.
	 * @param {Object} m - object with subobjects representing assignment paths.
	 * @param {number} amt - amount multiplier for any assignments.
	 */
	subeffect( obj, m, amt ) {

		if ( typeof obj !== 'object' ) {
			return;
		}

		for( let p in m ) {

			let target = obj[p];
			let source = m[p];

			if ( source && typeof source === 'object' ) {
				if ( source.isRVal ) {
					// Converting all rvalue sources into a number to prevent infinite recursion
					source = +source;
				} else if ( source.constructor.name !== 'Object' ) {
					console.warn( 'Class instance source in subeffect', source );
				}
			}

			if ( typeof source === 'object' ) {
				this.subeffect( target, source, amt );
			} else {
				if ( target instanceof Stat ) {

					target.base += source * amt;

				} else if ( typeof target === 'object') {

					target.value = ( target.value || 0 ) + source * amt;

				} else obj[p] += source * amt;

			}

		}

	},

	/**
	 * Add new sub-object to this object.
	 * Vue reactivity??
	 * @todo
	 * @param {Object} obj - parent object.
	 * @param {string} key - prop key to set.
	 * @param {Object} mod - modify amount.
	 * @param {number} [amt=1] - times modifier applied.
	 */
	newSub( obj, key, mod, amt=1 ) {

		console.warn( 'ADD SUB: ' + this.id + ' ' + key + ' stat: ' + (amt*mod.value) );

		let s = obj[key] = new Stat( typeof mod === 'number' ? mod*amt : 0, 'key' );
		if ( mod instanceof Mod ) s.apply( mod, amt );

	},

	/**
	 * Modify a mod applied by the Item.
	 * @param {Object|Mod|number} mod
	 * @param {number} amt - percent of change applied to modifier.
	 */
	changeMod( mod, amt, scale=true ) {

		// apply change to modifier for existing item amount.
		Game.applyMods( mod, scale ? amt*this.value : amt );

	},

	/**
	 * Add tag to object.
	 * @param {string} tag
	 */
	addTag( tag ) {
		if ( Array.isArray(tag) ) tag.forEach( t => this.addTag( t ), this );

		else if ( this._tags == null ) this._tags = [ tag ];
		else if ( !this._tags.includes(tag) ) this._tags.push(tag);
	},

	/**
	 *
	 * @param {string} t - tag to test.
	 * @returns {boolean}
	 */
	hasTag( t ) { return this.tags && this.tags.includes(t); },

	/**
	 * Test if item has every tag in list.
	 * @param {string[]} a - array of tags to test.
	 * @returns {boolean}
	*/
	hasTags( a ) {

		if ( !this._tags ) return false;
		for( let i = a.length-1; i >= 0; i-- ) if ( !this._tags.includes(a[i]) ) return false;

		return true;

	},

	canUse(){return true;},

	/**
	 * Test if tag has any tag in the list.
	 * @param {string[]} a - array of tags to test.
	 * @returns {boolean}
	 */
	/*anyTag( a ) {

		if ( !this._tags ) return false;
		for( let i = a.length-1; i >= 0; i-- ) if ( !this._tags.includes(a[i]) ) return true;

		return false;

	},*/

}